# Protocolo Equipo AVL vRaptor

## Protocolo de Comunicación Equipo-Aplicación

Ver Protocolo ODC Equipo Azul v1.14.

## Comandos

Comando     | Tipo de paquete             | Firm | Descrípción
:---------: | :-------------------------: | :--: | :----------
[HA](#ha)   | Handshake (saludo inicial)  | 1.10 | Poner HB, HA2
[Q2](#q2)   | Posición                    | 1.10 | RQ2
[EV](#ev)   | Evento                      | 1.10 | REV
[EV](#ev)   | Exceso de velocidad         | 1.10 | Evento especial paquete exceso de velocidad (no implementado)
[EV](#ev)   | Inspector sube/baja         | 1.10 | 
[CP](#cp)   | Configuración de parámetros | 1.10 | 
[CM](#cm)   | Configuración de mensajes   | 1.10 | 
[DD](#dd)   | Display de mensajes         | 1.10 | 
[DM](#dm)   | Borrado de mensajes         | 1.10 | 
[RS](#dm)   | Reinicio del equipo         | 1.10 | 

## Códigos de Eventos

Código      | Firm | Descripción
:---------- | :--: | :----------
A001 a A088 | 1.10 | Mensajes de consola de usuario
F402        | 1.16 | Inspector sube
F403        | 1.16 | Inspector baja
F406        | 1.16 | Personal ingresa
F407        | 1.16 | Personal egresa

**Eventos pendientes a implementar (validar código tras cada integración)**

- F401: Velocidad máxima superada
- B010: En contacto + apagado/motor detenido
- B011: En contacto
- B021: Ignición

Viejos códigos (según protocolo ODC Azul):  

- B011: Oprimió botón de pánico
- B010: Liberó botón de pánico
- B013: Sirena activa (Para aplicaciones de ambulancias)
- B012: Sirena Inactiva (Para aplicaciones de ambulancias)
- B015: Puerta abierta (Para aplicaciones de ambulancias)
- B014: Puerta cerrada (Para aplicaciones de ambulancias)
- B021: Se encendió el motor. (Para aplicaciones de ambulancias)
- B020: Se apagó el motor (Para aplicaciones de ambulancias)
- B030: Se desconectó batería principal (solo aplicable donde el equipo lleve batería propia)
- B031: Se reconectó batería principal (solo aplicable donde el equipo lleve batería propia, suponiendo que al reconectar la batería todavía está funcionando).
- B041: Combustible cortado por el equipo
- B040: Combustible habilitado por el equipo
- B100: Detenido (por debajo de velocidad mínima durante tiempo programado)
- F407: Evento de arranque
- B018: Oprimió 9 temporizado (cuando no hay botón de pánico)
- D000: Mensaje leído por el chofer

## Protocolo de Comandos

**Formato del mensaje**  
Todas las comunicaciones utilizan caracteres ASCII imprimibles y en mayúsculas. Cada sentencia tiene el siguiente formato:  
\#TABBB:{C}[;ID=DDDDD][;*FF]\#  

Ref     | Descripción
:------ | :----------
\#      | Comienzo de nuevo comando.
`T`     | Tipo de comando.
`A`     | Grupo de comando.
`BBB`   | Identificador de comando.
`C`     | String de datos.
`DDDDD` | Opcional: ID de vehiculo 5 char).
`FF`    | Opcional: Checksum (2 char).
\#      | Caracter delimitador.
\{x\}   | Significa que x puede ocurrir cualquier cantidad de veces, o data opcional.
\[x\]   | Significa que x puede ocurrir opcionalmente una vez.

**Tipos de comandos**
- `S` Set: escribe parámetros  
- `Q` Query: consulta parámetros  
- `R` Response: respuesta a comandos de escritura (S) o consulta (Q)  

**Grupos de comandos**  
Los comandos son agrupados según la funcionalidad. Así se tienen los siguientes grupos:
- `V`: Comandos de equipo básicos.
- `C`: Comandos de conectividad.
- `A`: Comandos de audio.

**Consideraciones**  
- Comandos SMS: se deben enviar como texto simple por SMS a la línea celular del equipo destino, anteponiendo el caracter `#`.
- `*` indica que la longitud del string, tanto en opciones de configuración como de respuesta, es variable. Se debe considerar los rangos de valores admitidos.


## Datos <a name="eeprom_data"></a> memoria no volátil

A continuación se listan todos los datos que se alojan en memoria no volátil del equipo.

Funcionalidad    | Parámetro     | Firm | Descripción
:--------------- | :------------ | :--: | :----------
TRACKER          | serial        | 1.22 | Serial number VRX.
REPORTE          | nPosPerPkg    | 1.22 | Cantidad de datos de posición por paquete de reporte a servidor.
REPORTE          | periodTxPos   | 1.22 | Periodo de tiempo de adquisición de datos de posición.
REPORTE          | txRetryPer    | 1.22 | Periodos de tiempo entre reintentos de envíos de paquetes ante no conexión contra servidor.
CONECTIVIDAD     | servDestStr   | 1.22 | Servidor de reporte (0, 1).
CONECTIVIDAD     | servPortStr   | 1.22 | Puerto de reporte (0, 1).
CONECTIVIDAD     | servDestPri   | 1.22 | Servidor:puerto de reporte principal (0, 1).
CONECTIVIDAD     | apnDestStr    | 1.22 | APN destino (0, 1).
CONECTIVIDAD     | apnUserStr    | 1.22 | APN usuario (0, 1).
CONECTIVIDAD     | apnPassStr    | 1.22 | APN password (0, 1).
CONECTIVIDAD     | apnDestPri    | 1.22 | APN principal (0, 1).
INTERFAZ USUARIO | msgEventCode  | 1.22 | Código de evento (tabla de mensajes).
INTERFAZ USUARIO | msgFlagDisp   | 1.22 | Flag display mensaje (tabla de mensajes).
INTERFAZ USUARIO | msgMsg        | 1.22 | Mensaje predefinido (tabla de mensajes).
INTERFAZ USUARIO | msgFlagTabV   | 1.22 | Flag tabla válida (tabla de mensajes).
INTERFAZ USUARIO | timeOnLed     | 1.22 | Tiempo de encendido de led estado de vehículo.
INTERFAZ USUARIO | timeoutBL     | 1.22 | Timeout encendido backlight display.
INTERFAZ USUARIO | timeoutMsg    | 1.27 | Timeout eliminación mensaje personalizado en display.
AUDIO            | strSpkLvl     | 1.22 | Ganancia amp speaker.
AUDIO            | strRxGain     | 1.22 | Ganancia preamp speaker.
AUDIO            | strMicLvl     | 1.22 | Ganancia micrófono.
FTP & FOTA       | sFtpServ      | 1.22 | FTP server.
FTP & FOTA       | sFtpPort      | 1.22 | FTP port.
FTP & FOTA       | sFtpUser      | 1.22 | FTP user.
FTP & FOTA       | sFtpPass      | 1.22 | FTP password.

Información adicional:  
- El periodo efectivo de reporte de datos a servidor se conforma según: nPosPerPkg x periodTxPos. Ej., si: nPosPerPkg = 5 y periodTxPos = 10, el periodo en que se reportan datos al servidor es de 50 segundos.  


## Parámetros configurables (no volátiles) <a name="param_data"></a>

Se configuran mediante:  
- Comando desde aplicación (a respuesta de evento): CP;MSGID;IDMOVIL;NOMBRE;VALOR;VCONFIG  
- Comando SMS: ver [VRCP](#vrcp)

Nombre  | Firm | Rango     | Unidad    | Default   | Descripción
:------ | :--: | :-------- | :-------- | :-------- | :----------
pos_per | 1.22 | 10~3600   | seg       | 10        | Periodo de tiempo de adquisición de datos de posición.
pos_num | 1.22 | 1~5       | -         | 5         | Cantidad de datos de posición por paquete de reporte a servidor.
tim_led | 1.22 | 0~255     | min       | 0         | Timeout Led testigo ON.
tim_bkl | 1.22 | 15~255    | seg       | 60        | Timeout Backlight ON.
tim_msg | 1.26 | 0~255     | min       | 5         | Timeout Mensaje de usuario en display.
srv_rep | 1.22 | -         | -         | -         | Servidor de reporte.
apn_rep | 1.22 | -         | -         | -         | APN de reporte.
vel_max | 1.22 | 0~255     | kmh       | 255       | Umbral velocidad máxima para generación de evento.

Información adicional:  
- El periodo efectivo de reporte de datos a servidor nunca puede ser menor a 10 segundos.  


# Comandos - Resumen

## Comandos básicos

Comando        | Opciones | Firm | Descrípción
:------------: | :------: | :--: | :----------
[VTEC](#vtec)  | Q___R    | 1.10 | Consulta de datos técnicos del equipo.
[VSLV](#vslv)  | Q___R    | 1.10 | Consulta de nivel de señal GSM y GPS.
[VRST](#vrst)  | __S_R    | 1.10 | Reset de equipo.
[VRSC](#vrsc)  | Q___R    | 1.10 | Consulta de contadores de reset y de transmisión de datos a servidor.
[VRCP](#vrcp)  | Q_S_R    | 1.27 | Consulta y configuración de parámetros no volátiles del equipo.

## Comandos de conectividad

Comando        | Opciones | Firm | Descrípción
:------------: | :------: | :--: | :----------
[CGIP](#cgip)  | Q_S_R    | 1.15 | Consulta y configuracion de servidores (IP/NOMBRE:PORT) de reporte de paquetes de datos.
[CAPN](#capn)  | Q_S_R    | 1.15 | Consulta y configuracion de APNs.

## Comandos de audio

Comando        | Opciones | Firm | Descrípción
:------------: | :------: | :--: | :----------
[ASPK](#aspk)  | Q_S_R    | 1.11 | Consulta y configuración del nivel de volumen del speaker.
[AMIC](#amic)  | Q_S_R    | 1.11 | Consulta y configuración del nivel de ganancia del micrófono.

## Comandos admin (user)

Comando        | Opciones | Firm | Descrípción
:------------: | :------: | :--: | :----------
[UFWU](#ufwu)  | __S_R    | 1.20 | Update de firmware over-the-air (FOTA).


# Comandos - Detalle

## <a name="vtec"></a> VTEC

**Descripción**  
Consulta de datos técnicos del equipo.  

**Configuración**  
No se admite.  

**Consulta**  
QVTEC  

**Respuesta**  
RVTEC:IMEI>`aaaaaaaaaaaaaaaa`;FW>`bbbb`;ID>`ccccc`;SN>`dddddddddd`#  

Donde:  
`aaaaaaaaaaaaaaaa` IMEI del modem celular del equipo.  
`bbbb` Versión de firmware del equipo.  
`ccccc` ID del equipo.  
`dddddddddd` Serial del equipo.  

**Ejemplo**  
SMS: #QVTEC  
RTA: #RVTEC:IMEI>867060033876448;FW>1.13;ID>04185;SN>AA-DV10001#  


## <a name="vslv"></a> VSLV

**Descripción**  
Consulta de nivel de señal GSM y GPS.  

**Configuración**  
No se admite.  

**Consulta**  
QVSLV  

**Respuesta**  
RVSLV:GSM>`a*`(`b*`);GPS>HDOP:`c*`;FIX:`dd`;NSAT:`ee`#  

Donde:  
`a*` Nivel de señal GSM -RSSI- [0-199].  
`b*` Indicador de referencia de señal GSM: EXCELLENT, GOOD, FAIR y POOR.  
`c*` Horizontal dilution of position [0.5-99.9].  
`dd` Fix [2D; 3D].  
`ee` Cantidad de satélites [00-12].  

Excepciones:  
En caso de error en consulta GSM, la respuesta es `UNKNOWN`.  
En caso de no obtener posición GPS, la respuesta es `NO FIX`.  
En caso de error al consultar posición GPS, la respuesta es `ERROR FIX`.  

**Ejemplo**  
SMS: #QVSLV  
RTA: #RVSLV:GSM>Rssi:28(EXCELLENT);GPS>hdop:1.2,fix:,nsat:06#  


## <a name="vrst"></a> VRST

**Descripción**  
Reinicializa módulo GSM/GNSS y comunicaciones.  

**Configuración**  
SVRST  

**Consulta**  
No se admite.  

**Respuesta**  
RVRST:OK#  

Donde:  
`OK` Si reset se ejecutó correctamente. Caso contrario, equipo no envia respuesta.  

**Ejemplo**  
SMS: #SVRST  
RTA: #RVRST:OK#  


## <a name="vrsc"></a> VRSC

**Descripción**  
Consulta de contadores de reset y de transmisión de datos a servidor.  

**Configuración**  
No se admite.  

**Consulta**  
QVRSC  

**Respuesta**  
RVRSC:RESET TOTAL>`aaaaa`;RESET RS>`bbbbb`;TX TOTAL>`ccccc`;TX OK>`ddddd`;TX VALID>`eeeee`;TX ERROR>`fffff`#  

Donde:  
`aaaaa` Contador de resets globales del equipo.  
`bbbbb` Contador de resets por comando RS del equipo.  
`ccccc` Contador de transmisión de paquetes: intentos totales.  
`ddddd` Contador de transmisión de paquetes: enviados correctamente.  
`eeeee` Contador de transmisión de paquetes: enviados y de los cuales se obtuvo respuesta desde el servidor remoto.  
`fffff` Contador de transmisión de paquetes: paquetes de los que no se obtuvo respuesta desde el servidor remoto al ser enviados correctamente.  

**Ejemplo**  
SMS: #QVRSC  
RTA: #RVRSC:RESET>Tot:4,Rs:4,Svrst:0;TXDATA>Tot:1168,Ok:1167,Val:1145,Err:22#  


## <a name="vrcp"></a> VRCP

**Descripción**  
Consulta y configuración de parámetros no volátiles del equipo.  

**Configuración**  
SVRCP`a*`,`b*`  

Donde:
`a*` [Parámetro](#param_data).  
`b*` Valor a asignar.  

**Consulta**  
QVRCP`a*`  

**Respuesta**  
RVRCP:`a*`,`b*`#  

**Ejemplo**  
SMS: #SVRCPpos_per,120  
RTA: #RVRCP:pos_per,120#  


## <a name="cgip"></a> CGIP

**Descripción**  
Consulta y configuracion de servidores (IP/NOMBRE:PORT) de reporte de paquetes de datos.  

**Configuración**  
SCGIP`n`,`a*`,`b*`,`c`  
SCGIP`n`*  

Donde:  
`n` Índice servidor a modificar [0; 1].  
`a*` IP/NOMBRE servidor [máx 27 char].  
`b*` PORT servidor [0-65535].  
`c` Indica si se configura como servidor primario [0: sin efecto; 1: servidor primario].  

Excepciones:  
En caso de que `n` se encuentre acompañada de un `*`, se modifica la acción del comando, provocando que el servidor primario pase a ser el asignado según `n`.  
En caso de que se modifique el Servidor de reporte primario, el equipo lo configura inmediatamente, y realiza nuevamente el handshake.  

**Consulta**  
QCGIP  

**Respuesta**  
RCGIP:SERVdest(`n`),`a*`,`b*` {(`*`)}#  

Donde:  
`n` Índice de servidor (se muestran todos los servidores configurados).  
`a*` IP/NOMBRE servidor.  
`b*` PORT servidor.  
`*` Indica servidor primario activo.  

**Ejemplo**  
Establece servidor "0" de reporte sta-soltec.com:2000 como destino primario.  
SMS: #SCGIP0,sta-soltec.com,2000,1  
RTA: #RCGIP:SERVdest(0):"sta-soltec.com",2000 (\*)#  

Establece servidor "1" como servidor de reporte principal.  
SMS: #SCGIP1*  
RTA: #RCGIP:SERVdest(1):"sta-soltec.com",2000 (\*)#  


## <a name="capn"></a> CAPN

**Descripción**  
Consulta y configuracion de APNs.  

**Configuración**  
SCAPN`n`,`a*`,`b*`,`c*`,`d`  
SCAPN`n`*  

Donde:  
`n` Índice de APN a modificar [0; 1].  
`a*` APN destino [máx 27 char].  
`b*` Usuario [máx 13 char].  
`c*` Contraseña [máx 13 char].   
`d` Indica si se configura como APN primario [0: sin efecto; 1: APN primario].  

Información adicional:  
En caso de que `n` se encuentre acompañada de un `*`, se modifica la acción del comando, provocando que el APN primario pase a ser el asignado según `n`.  
En caso de que se modifique el APN primario, el equipo lo configura inmediatamente.  

**Consulta**  
QCAPN  

**Respuesta**  
RCAPN:APNdest(`n`),`a*`,`b*`,`c*` {(`*`)}#  

Donde:  
`n` Índice de APN (se muestran todos los APN configurados).  
`a*` APN destino.  
`b*` Usuario.  
`c*` Contraseña.  
`*` Indica APN primario activo.  

**Ejemplo**  
Establece APN "0" "igprs.claro.com.ar", user "clarogprs", pass "clarogprs999", como APN primario.  
SMS: #SCAPN0,igprs.claro.com.ar,clarogprs,clarogprs999,1  
RTA: #RCGIP:SERVdest(0):"sta-soltec.com",2000 (\*)#  

Establece APN "1" como APN principal.  
SMS: #SCAPN1*  
RTA: #RCAPN:APNdest(1):"wap.gprs.unifon.com.ar","wap","wap" (\*)#  


## <a name="aspk"></a> ASPK

**Descripción**  
Consulta y configuración del nivel de volumen del speaker.  

**Configuración**  
SASPK`a`  

**Consulta**  
QASPK  

**Respuesta**  
RASPK:`a`#  

Donde:  
`a` Nivel de volumen del speaker [0-5].  

**Ejemplo**  
SMS: #SASPK5  
RTA: #RASPK:5#  


## <a name="amic"></a> AMIC

**Descripción**  
Consulta y configuración del nivel de ganancia del micrófono.  

**Configuración**  
SAMIC`a*`  

**Consulta**  
QAMIC  

**Respuesta**  
RAMIC:`a*`#  

Donde:  
`a*` Nivel de ganancia del micrófono [0-65535].  

**Ejemplo**  
SMS: #QAMIC  
RTA: #RAMIC:10000#  


## <a name="ufwu"></a> UFWU

**Descripción**  
Update de firmware over-the-air (OTA). El proceso de actualización exitoso es:  
1. APP_CORE: Login en FTP server.
2. APP_CORE: Búsqueda y validación de archivo de actualización (su versión debe ser distinta a la propia del equipo).
3. APP_CORE: Descarga del archivo en la memoria del módulo de telecomunicaciones.
4. APP_CORE: Guardado del archivo en la memoria FLASH del microcontrolador.
5. APP_CORE: Validación de la integridad del archivo guardado en memoria y activación de flag de update.
6. APP_CORE: Reinicio del equipo (para iniciar BOOTLOADER).
7. BOOTLOADER: Validación de flag de update.
8. BOOTLOADER: Regrabado de archivo binario descargado en memoria flash de APP_CORE.
9. BOOTLOADER: Validación de integridad de mapa de memoria flash de APP_CORE.
10. BOOTLOADER: Salto a inicio de ejecución de aplicación de APP_CORE.
11. APP_CORE: Ejecución de aplicación -nueva versión-.

Información adicional:  
El proceso de update, en caso de ser exitoso, puede demorar aproximadamente entre 2 y 3 minutos.  
Los paquetes de datos encolados para su transmisión (dado por no existir conectividad), y en caso de que existiesen, se pierden en este proceso.  
Los datos guardados memoria EEPROM se conservan luego de este proceso. Esto aplica a las configuraciones guardadas en [memoria no volátil](#eeprom_data).  

**Configuración**  
SUFWU  

**Consulta**  
No se admite.  

**Respuesta**  
SUFWU:OK#  

Donde:  
`OK` Si comando fue recepcionado correctamente, previo a ejecución del proceso de update. Se deberá enviar un comando de consulta luego del update, para validar la nueva versión del dispositivo (si es que el dispositivo consiguií actualizarse correctamente).  

**Ejemplo**  
SMS: #SUFWU  
RTA: #RUFWU:OK#  
